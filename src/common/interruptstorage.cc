/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                               I N T E R R U P T _ S T O R A G E                               * 
 *                                                                                               * 
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
 \* * * * * * * * * * * * * * * * * * * * * * * * */
#include "common/interruptstorage.h"
#include "object/keyboard.h"



/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    METHODS                      # 
 \* * * * * * * * * * * * * * * * * * * * * * * * */

InterruptStorage::InterruptStorage() {
	// set all interrupts as panic object
	for (int i = 0; i < (MAX_INTERRUPT_NUMBER - MIN_INTERRUPT_NUMBER); ++i) {
		InterList[i] = &PanicObject;
	}
}

void InterruptStorage::assign(int iNum, InterruptHandler& handler) {
	InterList[iNum - 32] = &handler;
}

/** \todo implement **/
void InterruptStorage::handle(int iNum) {
	PanicObject.currentInterruptP(iNum);

	switch (iNum) {
	case 32:	// timer

		break;
	case 33:	// keyboard
		keyboard.trigger();
		break;
	case 34:	// pic2

		break;
	case 35:	// serial2

		break;
	case 36:	// serial1

		break;
	case 37:	// soundcard

		break;
	case 38:	// floppy

		break;
	case 39:	// parallelport

		break;
	case 40:	// rtc

		break;
	case 41:	// misc

		break;
	case 42:	// ata4

		break;
	case 43:	// ata3

		break;
	case 44:	// secondps2

		break;
	case 45:	// fpu

		break;
	case 46:	// ata1

		break;
	case 47:	// ata2

		break;
	default:
		break;
	}
}
