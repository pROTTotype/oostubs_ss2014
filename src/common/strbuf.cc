/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                 Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                 S T R I N G B U F F E R                                       *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "common/strbuf.h"

Stringbuffer::Stringbuffer() {
	buffer = 0;
	b_length = 0;
}

Stringbuffer::~Stringbuffer() {
}

void Stringbuffer::put(char c) {
	buffer[b_length++] = c;
	if ((b_length == BUFFERSIZE) || c == '\n') {		// if buffer is filled out or user pressed enter
		flush();
		b_length = 0;							// reset buffer length index
	}
}
