/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                 Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                      O _ S T R E A M                                          *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "common/o_stream.h"

O_Stream::O_Stream() :
		Stringbuffer(), fgColor(WHITE), bgColor(BLACK), blink(false), base(dec) {
}

O_Stream::~O_Stream() {
	base = dec;
}

O_Stream& O_Stream::operator <<(char value) {
	put(value);
	return *this;
}

O_Stream& O_Stream::operator <<(unsigned char value) {
	return *this << (char) value;
}

O_Stream& O_Stream::operator <<(char* value) {
	return *this << (const char*) value;
}

O_Stream& O_Stream::operator <<(const char* value) {
	while (*value != 0) {
		put(*value++);
	}
	return *this;
}

O_Stream& O_Stream::operator <<(short value) {
	return *this << (long) value;
}

O_Stream& O_Stream::operator <<(unsigned short value) {
	return *this << (unsigned long) value;
}

O_Stream& O_Stream::operator <<(int value) {
	return *this << (long) value;
}

O_Stream& O_Stream::operator <<(unsigned int value) {
	return *this << (unsigned long) value;

}

O_Stream& O_Stream::operator <<(long value) {
	// show negative value with minus symbol;
	if (value < 0) {
		*this << '-';
		value = value * (-1);
	}

	*this << (unsigned long) value;

	return *this;
}

O_Stream& O_Stream::operator <<(unsigned long value) {
	// transform value in hexadecimal
	if (value < base) {
		if (value < 10) {
			put((char) ('0' + value));
		} else {
			put((char) ('A' + value - 10));
		}
	} else {
		*this << (unsigned long) (value / base);
		*this << (unsigned long) (value % base);
	}

	return *this;
}

/** \brief \todo implement **/
O_Stream& O_Stream::operator <<(void* value) {
	Base oldbase = base;
	base = hex;
	*this << (unsigned long) value;
	base = oldbase;
	return *this;

}

O_Stream& O_Stream::operator <<(FGColor color) {
	flush();
	fgColor = color.color;
	setAttributes(fgColor, bgColor, blink);
	return *this;
}

O_Stream& O_Stream::operator <<(BGColor color) {
	flush();
	bgColor = color.color;
	setAttributes(fgColor, bgColor, blink);
	return *this;
}

O_Stream& O_Stream::operator <<(Blink blink) {
	flush();
	this->blink = blink.blink;
	setAttributes(fgColor, bgColor, this->blink);
	return *this;
}

O_Stream& endl(O_Stream& os) {
	os << '\n';
	os.flush();
	return os;
}

O_Stream& bin(O_Stream& os) {
	os.base = O_Stream::bin;
	return os;
}

O_Stream& oct(O_Stream& os) {
	os.base = O_Stream::oct;
	return os;
}

O_Stream& dec(O_Stream& os) {
	os.base = O_Stream::dec;
	return os;
}

O_Stream& hex(O_Stream& os) {
	os.base = O_Stream::hex;
	return os;
}

O_Stream& O_Stream::operator <<(O_Stream& (*f)(O_Stream&)) {
	return f(*this);
}
