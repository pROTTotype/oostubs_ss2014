/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                                          P A N I C                                            * 
 *                                                                                               * 
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
 \* * * * * * * * * * * * * * * * * * * * * * * * */
#include "common/panic.h"
#include "object/kout.h"
#include "object/cpu.h"



/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    METHODS                      # 
 \* * * * * * * * * * * * * * * * * * * * * * * * */

/** \todo implement **/
void Panic::trigger() {
	cpu.disable_int();

	kout << "unhandled interrupt: " << actualInterruptNumber << " -> abort" << endl;
	cpu.halt();

}

/** \todo implement **/
void Panic::currentInterruptP(int iNum) {
	actualInterruptNumber = iNum;

}
