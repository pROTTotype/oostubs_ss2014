/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                               I N T E R R U P T _ M A N A G E R                               * 
 *                                                                                               * 
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
 \* * * * * * * * * * * * * * * * * * * * * * * * */
#include <device/interruptmanager.h>

void InterruptManager::assign(int iNum, InterruptHandler& handler) {
	InterStorage.assign(iNum, handler);

	switch (iNum) {
	case 32:
		PicObject.allow(PIC::timer);
		break;
	case 33:
		PicObject.allow(PIC::keyboard);
		break;
	case 34:
		PicObject.allow(PIC::pic2);
		break;
	case 35:
		PicObject.allow(PIC::serial2);
		break;
	case 36:
		PicObject.allow(PIC::serial1);
		break;
	case 37:
		PicObject.allow(PIC::soundcard);
		break;
	case 38:
		PicObject.allow(PIC::floppy);
		break;
	case 39:
		PicObject.allow(PIC::parallelport);
		break;
	case 40:
		PicObject.allow(PIC::rtc);
		break;
	case 41:
		PicObject.allow(PIC::misc);
		break;
	case 42:
		PicObject.allow(PIC::ata4);
		break;
	case 43:
		PicObject.allow(PIC::ata3);
		break;
	case 44:
		PicObject.allow(PIC::secondps2);
		break;
	case 45:
		PicObject.allow(PIC::fpu);
		break;
	case 46:
		PicObject.allow(PIC::ata1);
		break;
	case 47:
		PicObject.allow(PIC::ata2);
		break;
	default:
		break;
	}
}

