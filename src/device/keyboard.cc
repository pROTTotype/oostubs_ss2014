/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                                       K E Y B O A R D                                         * 
 *                                                                                               * 
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
 \* * * * * * * * * * * * * * * * * * * * * * * * */
#include "device/keyboard.h"
#include "object/kout.h"
#include "object/keyboard.h"
#include "object/cpu.h"
#include "config.h"
#include "machine/keyctrl.h"
#include "object/imanager.h"

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    METHODS                      # 
 \* * * * * * * * * * * * * * * * * * * * * * * * */

void Keyboard::plugin() {
	iManager.assign(33, *this);
}

/** \todo implement **/
void Keyboard::trigger() {
		Key k = keys.key_hit();
		if (k.valid()){

			// Press Ctrl + Alt + Delete for Reboot
			if (k.ctrl() && k.alt() && k.scancode() == 0x53 ) {
					keys.reboot();
			}
			// normal keys
			else {
					kout.flush();
					unsigned short x,y;
					kout.getpos(x, y);
					kout.setpos(KEYBOARD_X, KEYBOARD_Y);
					kout << (unsigned char) k.ascii() << endl;
					kout.setpos(x, y);

		}
		}

}

