/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *            		               Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                    C G A _ S C R E E N                                        *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "machine/cgascr.h"
#include "machine/io_port.h"

static IO_Port cga_0x3d4(0x3d4);	// Port: 3d4 index register (write only)
static IO_Port cga_0x3d5(0x3d5);	// Port: 3d5 data  register (read and write)
static char *SCREEN;				// Char Pointer for display
int setAttributesResult = 0x07;


CGA_Screen::CGA_Screen() {
	SCREEN = (char*) 0xb8000;		// set pointer to the valid adress
	setpos(0, 0);					// set cursor the first position;
	clear();

}

CGA_Screen::~CGA_Screen() {
}

void CGA_Screen::clear() {
	for (unsigned short i = 0; i < MAX_X * MAX_Y * 2; i++) {
		SCREEN[i] = ' ';
		i++;
		SCREEN[i] = bgcolor;
	} // for
}

void CGA_Screen::show(unsigned short x, unsigned short y, char character, unsigned char attribute) {
	if ((x < 80) && (y < 25)) {
		unsigned short reg_num = get_reg_num(x, y);
		SCREEN[reg_num] = character;
		SCREEN[reg_num + 1] = attribute;
	}
}

void CGA_Screen::getpos(unsigned short& x, unsigned short& y) const {
	cga_0x3d4.outb(14);
	unsigned int pos = ((unsigned int) cga_0x3d5.inb()) << 8; // read value from control register

	cga_0x3d4.outb(15);
	pos += (unsigned int) cga_0x3d5.inb(); 					// read value from control register

	x = (unsigned short) pos % MAX_X;
	y = (unsigned short) pos / MAX_X;

}

void CGA_Screen::setpos(unsigned short x, unsigned short y) {
	// invalid x value -> calculate new coordinate
	if (x >= 80) {
		y += x / 80;
		x = x % 80;
		if (y >= 25) {
			scrollup();
			y--;
		}
	}

	unsigned short pos = y * 80 + x;
	if ((x < 80) && (y < 25)) {
		cga_0x3d4.outb(14);
		cga_0x3d5.outb(pos >> 8);	// write value in control register

		cga_0x3d4.outb(15);
		cga_0x3d5.outb(pos);		// write value in control register
	}
}

void CGA_Screen::scrollup() {
	unsigned short x;
	unsigned short y;

	getpos(x, y);

	for (int k = 0; k < MAX_X * MAX_Y * 2; k++) {
		SCREEN[k] = SCREEN[k + 160];
	} // for
}

void CGA_Screen::print(const char* string, unsigned int n) {
	unsigned short x, y;

	for (unsigned int i = 0; i < n; i++) {
		getpos(x, y);
		if (string[i] == '\n') {
			x = 0;
			if (y == 24) {
				scrollup();
			} else {
				y++;
			}
			setpos(x, y);
		} else {
			show(x, y, string[i], setAttributesResult);
			setpos(x + 1, y);
		}
	} // for
}

/** \todo implement **/
void CGA_Screen::setAttributes(int fgColor, int bgColor, bool blink) {
	unsigned short x;
	unsigned short y;

	getpos(x, y);

	setAttributesResult = fgColor;
	setAttributesResult |= bgColor << 4;
	setAttributesResult |= blink << 7;

	unsigned short reg_num = get_reg_num(x, y);

	SCREEN[reg_num + 1] = setAttributesResult;
}
