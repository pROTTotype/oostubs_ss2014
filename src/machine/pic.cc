/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                                            P I C                                              * 
 *                                                                                               * 
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
\* * * * * * * * * * * * * * * * * * * * * * * * */
#include "machine/pic.h"
#include "machine/io_port.h"
#include "object/cpu.h"


/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    METHODS                      # 
\* * * * * * * * * * * * * * * * * * * * * * * * */

/** \todo extend - add global interrupt eneabling after pic configuration **/
PIC::PIC() {
  IO_Port ctrl_1(0x20), ctrl_2(0xa0), mask_1(0x21), mask_2(0xa1);
  ctrl_1.outb(0x11);
  ctrl_2.outb(0x11);
  
  mask_1.outb(32);
  mask_2.outb(40);
  
  mask_1.outb(4);
  mask_2.outb(2);
  
  mask_1.outb(3);
  mask_2.outb(3);

  cpu.enable_int();
  mask_1.outb(0x00);
  mask_2.outb(0x00);

}

PIC::~PIC(){

}

/** \todo implement **/
void PIC::allow(Interrupts interrupt){
  // ToDo: your code goes here
	unsigned char allow;
	IO_Port mask_1(0x21), mask_2(0xa1);

	if (interrupt < 40) {
		allow = mask_1.inb();
		allow &= ~(1 << (interrupt - 32));
		mask_1.outb(allow);
	}
	else {
		allow = mask_2.inb();
		allow &= ~(1 << (interrupt - 40));
		mask_2.outb(allow);
	}
}

/** \todo implement **/
void PIC::forbid(Interrupts interrupt){
  // ToDo: your code goes here
	unsigned char forbid;
	IO_Port mask_1(0x21), mask_2(0xa1);


	if (interrupt < 40) {
		forbid = mask_1.inb();
		forbid |= (1 << (interrupt - 32));
		mask_1.outb(forbid);
	}
	else {
		forbid = mask_2.inb();
		forbid |= (1 << (interrupt - 40));
		mask_2.outb(forbid);
	}
}

/** \todo implement **/
void PIC::ack(Interrupts interrupt){
  // ToDo: your code goes here
	IO_Port ctrl_1(0x20), ctrl_2(0xa0);
	if (interrupt < 40) {
		ctrl_1.outb(0x20);
	}
	else {
		ctrl_2.outb(0x20);
	}

}
