/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                 Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                         S C H E D U L E R                                     *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   INCLUDES                      #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

#include "thread/scheduler.h"
#include "locking/scopedLock.h""
#include "object/log.h"
#include "object/cpu.h"

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   METHODS                       #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

Scheduler::Scheduler() :
		started(false) {
}

Scheduler::~Scheduler() {
	started = false;
}

void Scheduler::start() {
	log << "Start scheduler" << endl;
	Dispatcher::start(*static_cast<Thread*>(threads.pop_front()));
	started = true;
}

void Scheduler::insert(Thread& that) {
	log << "insert thread" << endl;
	threads.push_back(that);
}

void Scheduler::next() {
	Thread* next = static_cast<Thread*>(threads.pop_front());
	if (!next) {
		log << "No more threads, finished!" << endl;
		cpu.halt();
	}
	dispatch(*next);
}

void Scheduler::exit() {
	if (!threads.empty()) {
		Thread* t = static_cast<Thread*>(threads.pop_front());

		threads.pop_front();

		dispatch((*t));
	}
}

bool Scheduler::kill(Thread& that) {
	return threads.remove(that);

}

void Scheduler::yield() {
	if (!threads.empty()) {
		log << "yield" << endl;
		Thread* t = static_cast<Thread*>(threads.peek_front());
		threads.pop_front();
		threads.push_back(*t);

		dispatch((*t));
	} else
		log << "No more threads, finished!" << endl;

}

void Scheduler::preempt() {
	yield();
}
