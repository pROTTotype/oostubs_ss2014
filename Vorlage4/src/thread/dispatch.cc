/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                 Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                         D I S P A T C H E R                                   *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   INCLUDES                      #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

#include "thread/dispatch.h"
#include "object/log.h"

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   METHODS                       #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

void Dispatcher::start(Thread& first) {
	log << "Dispatcher start" << endl;
	current = &first;
	first.context.set();
}

void Dispatcher::dispatch(Thread& next) {
	// Helpvariable for legal variables
	Thread& tmp = *current;
	current = &next;
	tmp.context.swap(next.context);

}
