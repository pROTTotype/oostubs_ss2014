/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                 Technische Informatik II                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *                                         W A T C H                                             *
 *                                                                                               *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   INCLUDES                      #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

#include "device/watch.h"
#include "object/imanager.h"
#include "object/scheduler.h"
#include "object/log.h"
#include "object/lock.h"

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                   METHODS                       #
 \* * * * * * * * * * * * * * * * * * * * * * * * */

void Watch::windup(unsigned int ms) {
	PIT pit;
	pit.interval(ms);
	iManager.assign(PIC::timer, *this);
}

void Watch::trigger() {
	lock.unlock();
	scheduler.preempt();
	lock.lock();
}
