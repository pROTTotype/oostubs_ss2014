/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                   Technische Informatik II                                    * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                                               * 
 *                                       K E Y B O A R D                                         * 
 *                                                                                               * 
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    INCLUDES                     #
\* * * * * * * * * * * * * * * * * * * * * * * * */
#include "device/keyboard.h"
#include "object/imanager.h"
#include "object/kout.h"

/* * * * * * * * * * * * * * * * * * * * * * * * *\
#                    METHODS                      # 
\* * * * * * * * * * * * * * * * * * * * * * * * */
void Keyboard::plugin(){
  iManager.assign(InterruptManager::keyboard, *this);
}

void Keyboard::trigger(){
  //Var init
  Key k;
  
  //Taste abholen
  k = key_hit();
  if(k.valid()){
    //Neustarten?
    if(k.scancode()==Key::scan::del && k.alt() && k.ctrl()){
      //und Reset der CPU ausloesen
      reboot();
    }else{
      //Zeichen an fester Position ausgeben
      kout.setpos(KEYBOARD_Y, KEYBOARD_X);
      kout << k.ascii();
      kout.flush();
    }
  }
}
