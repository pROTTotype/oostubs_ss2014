/** \~german \page task5 Aufgabe 5 - Blockierendes Warten 
\tableofcontents
 * 
 * Die fünfte Aufgabe erweitert das OOstuBS um einen weiteren Thread-Zustand.
 * Bisher wurden "bereit" und "aktiv" auf der Ebene des Schedulers umgesetzt, 
 * nun kommt noch "blockiert" hinzu. Dazu wird eine weitere 
 * Abstraktionsebene über den Klassen \ref Thread und \ref Scheduler.  
 * eingefügt. Oberhalb der erstgenannten wird die Klasse \ref Customer eingefügt.
 * Eine Instanz dieser Klasse kann als blockiert markiert und in einer separaten 
 * Queue organisiert werden. Verantwortlich dafür ist der \ref Organizer, damit
 * das short-term-scheduling des \ref Scheduler und \ref Dispatcher um ein 
 * mid-term-scheduling erweitert. Dabei werden Prozesse, die synchronisiert werden
 * sollen oder auf eine I/O Operation warten, nicht an den Scheduler zur 
 * Ausführung übergeben und in seperaten Queues gespeichert. Die Entscheidung,
 * ob ein Thread bereit werden kann (weil eine bestimmte Ressourcen bereit-
 * steht) trifft ein ressourcenbezogener \ref Semaphore.
 * 
 * Die Idee dieses Mechanismus soll an der I/O Klasse \ref Keyboard verdeutlicht
 * werden. Die Klasse erbt nunmehr ,wie im Klassendiagramm ersichtlich, von der 
 * Klasse \ref Semaphore. Eine entsprechende Instanz wird im Konstruktor mit der
 * Zahl der verfügbaren Ressourcen (hier "0") initialisiert. Mit der Anforderung einer
 * Leseoperation unter \ref Keyboard::getkey(), die abhängig von den Eingaben 
 * des Nutzers beliebig lange dauern kann, wird also jeder Thread blockiert. Erfolgt
 * eine Eingabe, wird der Thread wieder dem Scheduler übergeben und bekommt 
 * im Falle der Ausführung das Ergebnis der getkey Funktion zugeordnet. Der Semaphore
 * synchronisiert also die Tastatureingaben und zugehörigen Threads.
 *
 * Die Vorlage umfasst die Implementierung einer kleinen Shell, unter der die 
 * 3 Anwendungen, Counter-1, Counter-2 und Rotating-Coursor in beliebiger 
 * Kombination gestartet und gestoppt werden können. Mit help kann eine Hilfe
 * zu diesen Möglichkeiten aufgerufen werden.
 *
 * Beachten Sie, dass das globale Objekt "scheduler" nunmehr eine Instanz der
 * Klasse \ref Organizer ist!
 *
 * <ol> 
 * <li>Erläutern Sie die Umsetzung des zuvor beschriebenen Algorithmus anhand 
 * des Klassendiagramms der KLasse \ref Keyboard !</li>
 * <li>Beschreiben Sie das Handling der eigentlichen Anwendungen. Analysieren 
 * Sie dazu die Klassen \ref Job und \ref JobManager. Was ist der Unterschied
 * zwischen einem \ref Customer und einem \ref Thread Objekt? Kann eine 
 * Applikation ohne jemals "bereit" gewesen zu sein, als "blockiert" behandelt
 * werden?</li>
 * <li>Erklären Sie welche Änderungen in der Klasse \ref Scheduler eingeführt
 * werden mussten, um das oben beschriebene Konzept umzusetzen.</li>
 * <li>Implementieren Sie die Methoden der Klassen \ref Semaphore und \ref 
 * Organizer.</li>
 * </ol>
 * 
 *
 *
 *
 * 
 *
 *
 *
 *
 * 
 * \~english \page task5 Task 5 - Blocking Wait
 *
 * The goal of the fifth task is to enable threads to block on I/O-Operations.
 * To support this the Curses_Keyboard is extended through a method getkey,
 * which delivers a key if one is available and blocks the calling thread until
 * a key is available. This functionality needs a synchronization mechanism
 * between individual threads and I/O-Drivers. OOStuBS uses semaphores to do
 * this synchronization. Therefore the existing scheduling mechanism needs to
 * be extended to represent the third state of the basic process model
 * "blocked".
 *
 * The existing class hierarchie is extended at two points, on one hand the
 * Scheduler is extended to the Organizer to handle blocking and wake up of
 * individual threads, on the other hand the Thread is extended to a Customer
 * to support tracking of the semaphore the customer is blocked in.
 *
 * The extended appraoch considers each customer to be present in either the
 * ready-queue of the scheduler, a waiting-queue of a semaphore or in the
 * current-pointer of the Dispatcher. Therefore semaphore needs a machanism to
 * track the customers waiting for their ressource. This is done through the
 * Waitingroom class. This class respresents the underlying waiting-queue of a
 * sempahrore object.
 *
 * Depending on the implementation of the Scheduler in Task 4 it may be
 * necessary to change some implementation details. There exist new situations,
 * which were not present in Task 4 one is an empty ready list and another is
 * the killing of a customer in any state(ready, running, blocked).
 *
 * Especially the problem of an empty ready-queue is not trivial to solve,
 * since Watch will issue Scheduler::resume   independant of the state of the
 * ready-list. Generally there exist to feasable solutions either the methods
 * of Scheduler are adapted to work even on empty ready-queues or a
 * non-terminatable low-priority Idle-Thread may be created, to prevent the
 * ready-queue from being empty.
 *
 * BUGS:
 *
 *  - If an additional thread is started it may happen that OOStuBS cannot be
 *  killed through CTRL-C or other signals. In this case please issue "make
 *  hardkill" in the Task5 folder of your oostubs in another terminal.
 *  Afterwards the terminal will be broken until you issue "reset".
 *
 **/

