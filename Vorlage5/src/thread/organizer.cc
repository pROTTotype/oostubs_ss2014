#include "thread/organizer.h"
#include "object/lock.h"
#include "locking/scopedLock.h"
#include "common/null.h"
#include "object/log.h"

void Organizer::block(Waitingroom& room) {
	room.push_back(*active());
	active()->waiting_in(&room);
	log << "Suspending Thread " << active() << endl;
	next();
}

void Organizer::wakeup(Customer& customer) {
	this->reactivate(customer);
	customer.waiting_in()->remove(customer);

	log << "Reactivating Thread " << &customer << endl;
}

bool Organizer::kill(Customer& that) {
	if (!that.waiting_in()) {
		return Scheduler::kill(that);
	} else {
		Waitingroom* w = that.waiting_in();
		w->remove(that);
		return true;

	}
	return true;

}

Customer* Organizer::active() const {
	return static_cast<Customer*>(Scheduler::active());
}
